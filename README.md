# MiniDebConf in a box

This application uses [wafer](https://github.com/CTPUG/wafer) and
[wafer-debconf](https://salsa.debian.org/debconf-team/public/websites/wafer-debconf/)
to provide a DebConf-style conference website, targetted at MiniDebConfs.

## Deploy checklist

This is Django project, so deploying it is very similar to deploying any other
Django project. The accompanying [Dockerfile](Dockerfile) provides a starting
point for everything that can be automated.

The following points are not automated at this point, and need to be done
manually:

- [ ] [register app on salsa](https://salsa.debian.org/profile/applications) so
      we can authenticate users against it.
- [ ] Set the `SITE_DESCRIPTION`, `SITE_AUTHOR`, `WAFER_GITLAB_CLIENT_ID` and
      `WAFER_GITLAB_CLIENT_SECRET` settings. You can do this in `localsettings.py`,
      or via environment variables.
- [ ] make sure that the server is able to send emails, as this is needed for
      users creating accounts without authenticating to salsa. If necessary,
      set the appropriate Django settings for outgoing email in
      `localsettings.py`.
      Check the [Django documentation](https://docs.djangoproject.com/en/2.2/topics/email/)
      for more information.
- [ ] If necessary, set the `TIME_ZONE` setting in `localsettings.py`.
- [ ] Set any other desired Django settings in `localsettings.py` (e.g.
      database connection info).
- [ ] Create an superuser account for you. You probably want login via Salsa
      first, and then make your account superuser in the Django console. Or you
      can create a superuser account with a password via the `createsuperuser`
      Django management command.
- [ ] In the Django admin interface, edit the default _Site_ and set the site
      name and URL.

## Customizing the layout

wafer-debconf already provides a basic layout, based on Bootstrap and SASS. To
provide custom CSS, create `static/_variables.scss` and
`static/_bootwatch.scss`. The first is included before bootstrap itself, and
can redefine bootstrap defaults; the former is included after bootstrap.  For
example, if you download those files from a
[Bootswatch](https://bootswatch.com/) theme and put them under `static/`, it
will just work.

Put any other extra static assets such as images under `static/`. In special,
you can override the logo in the navigation bar by providing
`static/img/logo.png`.

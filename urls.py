import re
from django.conf.urls import include, url
from django.views.static import serve
from django.conf import settings

urlpatterns = [
    url(r"", include("minidebconf.urls")),
    url(
        r"^%s(?P<path>.*)$" % re.escape(settings.MEDIA_URL.lstrip("/")),
        serve,
        {"document_root": settings.MEDIA_ROOT},
    ),
]
